<?php

return [
    'host' => '127.0.0.1',
    'dbname' => 'testDB',
    'user' => 'root',
    'password' => 'root',
];

/*

CREATE TABLE `testDB`.`employees`
( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT , `firstName` VARCHAR(32) NOT NULL ,
`lastName` VARCHAR(255) NOT NULL , `phone` VARCHAR(13) NOT NULL ,
`email` VARCHAR(32) NOT NULL ,
PRIMARY KEY (`id`))
ENGINE = InnoDB;

*/
