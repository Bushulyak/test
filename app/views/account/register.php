<div class="row">
    <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6 col-xl-6 center">
        <div class="card p10">
            <form class="form-signin" action="#" method="post" enctype="multipart/form-data">
                <h1>Регистрация</h1>
                <div class="form-group">
                    <label>Имя</label>
                    <input type="text" class="form-control" name="firstName" placeholder="Введите имя" required>
                </div>
                <div class="form-group">
                    <label>Фамилия</label>
                    <input type="text" class="form-control" name="lastName" placeholder="Введите фамилию" required>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" placeholder="Введите Email" required>
                </div>
                <div class="form-group">
                    <label>Номер телефона</label>
                    <!-- <input type="tel" class="form-control" name="phone" placeholder="Введите номер телефона" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" required> -->
                    <input type="tel" class="form-control" name="phone" placeholder="Введите номер телефона">
                </div>
                <div class="form-group">
                    <label>Пароль</label>
                    <input type="password" class="form-control" name="password" placeholder="Ведите пароль" required>
                </div>
                <div class="form-group">
                    <label>Повторите пароль</label>
                    <input type="password" class="form-control" name="password2" placeholder="Введите пароль повторно" required>
                </div>
                <div class="form-group">
                    <label>Дата рождения</label>
                    <input type="date" class="form-control" name="birthday" required>
                </div>
                <div class="form-group">
                    <label>Выберите фото</label><br>
                    <input type="file" name="photo">
                </div>
                <input type="submit" class="btn btn-success" name="register" value="Зарегистрироваться">
                <a href="/account/login" class="btn btn-primary">Авторизация</a>
            </form>
        </div>
    </div>
</div>
