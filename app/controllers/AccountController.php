<?php

namespace app\controllers;

use app\core\Controller;

class AccountController extends Controller {

    public function loginAction() {
        if (!empty($_POST)) {
            // $this->view->location('/');
            $this->view->message(200, 'Сообщение');
        }
        $this->view->render('Авторизация');
    }

    public function registerAction() {
        $this->view->render('Регистрация');
    }

}
