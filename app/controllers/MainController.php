<?php

namespace app\controllers;

use app\core\Controller;

class MainController extends Controller
{

    public function indexAction() {

        // $this->view->redirect('/contact');
        // $vars = [ 'name' => 'Виталий', 'age' => '24', 'arr' => [1, 2, 3] ];
        // $this->view->path = 'main/index';
        // $this->view->layout = 'default';
        // $this->view->render('Главная страница', $vars);

        // $db = new Db;
        // $params = [ 'id' => 1, 'email' => 'gbushulyak@gmail.com' ];
        // $data = $db->row('SELECT firstName, lastName, phone FROM employees WHERE id = :id AND email = :email', $params);
        // var_dump($data);

        $users = $this->model->getUsers();
        // var_dump($users);

        $this->view->render('Главная', [ 'users' => $users ]);

        // $this->view->render('Главная страница');
    }

    public function contactAction() {
        $this->view->render('Контакты');
    }

}
