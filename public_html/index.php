<?php
declare(strict_types=1);

// header('Content-Type: text/html; charset=utf-8');

require_once __DIR__ . '/../app/lib/Dev.php';

use app\core\Router;

spl_autoload_register(function($class) {
    $path = '../' . str_replace('\\', '/', $class . '.php');
    if (file_exists($path)) require $path;
});

session_start();

$router = new Router;
$router->run();
